#!/bin/bash

PANFORK_DIR="$(dirname "$0")"

add_ld_path() {
    if [ "$LD_LIBRARY_PATH" ]; then
        export LD_LIBRARY_PATH="$1":"$LD_LIBRARY_PATH"
    else
        export LD_LIBRARY_PATH="$1"
    fi
}

if [ "$1" = -h ] || [ "$1" = --help ]; then
    printf "Usage: %s [--system|--version VER] COMMAND\n" "$0"
    printf "See README.md for more information.\n"
    exit
elif [ "$1" = --system ]; then
    shift 1
    if [[ $LD_LIBRARY_PATH == *"panfork"* ]]; then
        export LD_LIBRARY_PATH="$(printf "%s" "$LD_LIBRAY_PATH" | tr : '\n' | grep -v panfork | tr '\n' : | head -c-1)"
    fi
    LDCONFIG=ldconfig
    if ! "$LDCONFIG" --help >/dev/null; then
        LDCONFIG=/sbin/ldconfig
    fi
    LIBPATH32="$(/sbin/ldconfig -p | grep libGL.so.1 | grep -v panfork | grep hard-float | head -n1 | cut -d">" -f2- | cut -d" " -f2-)"
    if [ "$LIBPATH32" ]; then
        add_ld_path "$(dirname -- "$LIBPATH32")"
    fi
    LIBPATH64="$(/sbin/ldconfig -p | grep libGL.so.1 | grep -v panfork | grep AArch64 | head -n1 | cut -d">" -f2- | cut -d" " -f2-)"
    if [ "$LIBPATH64" ]; then
        add_ld_path "$(dirname -- "$LIBPATH64")"
    fi
    exec "$@"
elif [ "$1" = --version ]; then
    VER="$2"
    shift 2
    ARGS=( "$@" )
    cd "$PANFORK_DIR"
    set -- --oldver "$VER" --shell
    . ./update.sh
    cd -
    "${ARGS[@]}"
else
    add_ld_path "$PANFORK_DIR"/arm
    add_ld_path "$PANFORK_DIR"/aarch64
    exec "$@"
fi
