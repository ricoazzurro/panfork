# panfork

A fork of the Panfrost graphics driver.

Probably you should be using upstream Mesa, as many of the fixes here
have now been merged there.

But upstreaming isn't instant, and so this fork still has a few
features not available there, such as significantly lower memory
usage, faster shader compilation on Bifrost, and support for Gallium
Nine.

(Thanks to Alyssa Rosenzweig for taking a number of commits from here
and merging them upstream when I wasn't able to do that myself.)

## Installation:

Clone or download this repository:

```sh
$ git clone https://gitlab.com/panfork/panfork
```

`cd` into the panfork, directory, then use the `update` script to
download the latest version:

```sh
$ ./update.sh
```

This will download the library to the `versions` directory, and
extract it to the `arm` directory and (on 64-bit systems) `aarch64`.

To use this as the OpenGL driver for all applications, add a snippet
like this to a file such as `~/.profile`, or for some desktop
environments (at least KDE) you can add an autostart script in the
system settings.

Remember to replace the `/path/to/panfork` with the full path to the
panfork repository. If you have a 32-bit or 64-bit only (no multilib)
system, delete the `add_ld_library_path` line for the architecture you
do not need.

```
add_ld_library_path() {
    if [ "$LD_LIBRARY_PATH" ]; then
        if ! echo "$LD_LIBRARY_PATH" | grep -Fqe "$1"; then
            export LD_LIBRARY_PATH="$1:$LD_LIBRARY_PATH"
        fi
    else
        export LD_LIBRARY_PATH="$1"
    fi
}
# For 64-bit distros
add_ld_library_path /path/to/panfork/aarch64
# For 32-bit distros
add_ld_library_path /path/to/panfork/arm
```

## Systemwide installation:

In most cases a per-user installation should be sufficient, but in
some cases you may want to install the library for all users on the
system.

As root, you can install it like this:

```sh
# ./update.sh --install /opt/panfork
```

Note that the `run.sh` tool depends on the installation path including
the string `panfork`.

For the system to find the libraries, you will need to add it to the
dynamic linker search path.

First make sure that the file `/etc/ld.so.conf` starts with a line
such as `include /etc/ld.so.conf.d/*.conf`.

Now create a file `/etc/ld.so.conf.d/00-panfork.conf`, and put in it
the paths to the library directories:

```
/opt/panfork/arm
/opt/panfork/aarch64
```

Finally run `ldconfig` as root to update the linker cache.

`ldd` should now show that `panfork` is being used as libGL:

```sh
$ ldd `which glxgears` | grep libGL.so
        libGL.so.1 => /opt/panfork/arm/libGL.so.1 (0xb6d06000)
```

To revert back to the system Mesa, remove the file
`/etc/ld.so.conf.d/00-panfork.conf` and run `ldconfig` again.

## Checking installation status:

Running `glxinfo | grep -E 'renderer string|OpenGL version'` should
print `Panfork` if it is installed correctly, otherwise it will show
`Panfrost`.

If you `grep` for the hex string after `git-` in `versions.csv`, it
should return what version you have installed:

```
$ grep 3de28f8818 versions.csv
```

## Running single applications with specific versions:

To temporally revert to the system Mesa installation:

```sh
$ ./run.sh --system glxgears
```

Note that this may cause some issues when Panfork is installed
systemwide.

To use a specific `panfork` version (see the downloaded `versions.csv`
file):

```sh
$ ./run.sh --version v0.0 glxgears
```

(This would place the libraries in `oldver/v0.0`, it is safe to remove
the directory afterwards to save storage space.)

If you have chosen not to install `panfork`, to use the currently
downloaded version:

```sh
$ ./run.sh glxgears
```

## Updating and reverting:

First make sure that the scripts are up to date:

```sh
$ git pull
```

If you did a systemwide installation, add `--install` with the
original install path to these commands.

To update to the latest version:

```sh
$ ./update.sh
```

To revert an update that broke things (please make an issue report!):

```sh
$ ./update.sh --revert
```

The file named similar to `reflog-1000` lists all the versions that
have been installed.

To update to a specific version (see `versions.csv` for the list):

```sh
$ ./update.sh --version v0.0
```

To update the version list without doing anything else:

```sh
$ ./update.sh --download-csv
```

## Reporting issues:

Problems with installing should be reported here in the
[Panfork repository](https://gitlab.com/panfork/panfork/-/issues).

Most graphical issues and other bugs should be reported
[upstream](https://gitlab.freedesktop.org/mesa/mesa), unless you think
the issue is fork-specific, in which case it may be better to create
an issue in our [Mesa
repository](https://gitlab.com/panfork/mesa/-/issues).

## Releases:

The binaries are automatically compiled by Gitlab CI when the source
repository is updated. New versions have to be registered in the
[panfork-versions](https://gitlab.com/panfork/panfork-versions)
repository before they show up in the installer.

Each release branch in the source repository (e.g. `v1`) is created
and then never rebased, using fixup commits if necessary.

The process for creating a new release branch is like this:

```
$ git checkout main                        # Start on the main branch
$ git rebase v1                            # Get any changes only in the release branch
$ git fetch upstream main                  # Download upstream changes
$ git rebase -i --autosquash upstream/main # Rebase on top of upstream, cleaning up our history
$ git switch -c v2                         # Create a new release branch
$ GIT_SEQUENCE_EDITOR="sed -Ei 's/pick ([^ ]* \*)/drop \1/'" git rebase -i upstream/main
# Remove work-in-progress commits from the release branch
$ git push fork v2                         # Upload the changes
$ git push fork main --force               # Update main branch, destorying history there
```
