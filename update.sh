#!/bin/bash

set -e

REFLOG=reflog-"$UID"

do_download() {
    if command -v curl &>/dev/null; then
        curl "$1" -o "$2"
    elif command -v wget &>/dev/null; then
        wget "$1" -O "$2"
    else
        echo "Couldn't find either wget or curl for downloading files"
        exit 1
    fi
}

download_versions() {
    do_download https://gitlab.com/panfork/panfork-versions/-/raw/main/versions.csv versions.csv
}

set_latest() {
    INFO=( $(grep -v SKIP versions.csv | tail -n1 | sed 's/,END,.*//' | tr , ' ') )
    NOTE="$(grep -v SKIP versions.csv | tail -n1 | sed 's/.*,END,//')"
}

set_version() {
    INFO=( $(grep -v SKIP versions.csv | grep -e "^$1," | tail -n1 | sed 's/,END,.*//' | tr , ' ') )
    NOTE="$(grep -v SKIP versions.csv | grep -e "^$1," | tail -n1 | sed 's/.*,END,//')"
}

set_revert() {
    set_version "$(uniq "$REFLOG" | tail -n2 | head -n1)"
}

set_current() {
    set_version "$(tail -n1 "$REFLOG")"
}

set_vars() {
    VERSION="${INFO[0]}"
    DATE="${INFO[1]}"
    COMMIT="${INFO[2]}"
    UPSTREAM="${INFO[3]}"
    ARM="${INFO[4]}"
    ARMSHA="${INFO[5]}"
    AARCH64="${INFO[6]}"
    AARCH64SHA="${INFO[7]}"
}

print_version() {
    printf "%s %s (commit %s), released %s: %s\n" "$1" "$VERSION" "${COMMIT::10}" "$DATE" "$NOTE"
}

download_file() {
    if ! [ -e "$4" ]; then
        echo Downloading latest $1 tarball
        do_download "https://gitlab.com/panfork/mesa/-/jobs/$2/artifacts/raw/panfork.tar.xz" "$4"
    else
        echo Tarball for $1 is already downloaded
    fi
    SHA="$(sha256sum "$4" | cut -d" " -f1)"
    if ! [ "$SHA" = "$3" ]; then
        echo "Error: SHA256SUM mismatch, corrupted file!"
        echo Deleting "$4"..
        rm "$4"
        echo Restart the update to try downloading it again
        exit 1
    fi
}

patch_dri_path() {
    OFFSET="$(sed '/DRI-DRIVERS-PATH/{s/DRI-DRIVERS-PATH.*//;q}' "$1" | wc -c)"
    END="$(sed '/DRI-DRIVERS-PATH-END/{s/\(DRI-DRIVERS-PATH-END\).*/\1/;q}' "$1" | wc -c)"
    LENGTH=$((END-OFFSET))
    if [ "$LENGTH" = 0 ]; then
        echo "DRI-DRIVERS-PATH not found in $1; not patching"
        return
    fi
    if [ "$LENGTH" -lt 40 ]; then
        echo "File $1 has already been relocated; not patching"
        DRIVERS_PATH_MSG=1
        return
    fi
    STRLEN=${#2}
    if [ "$STRLEN" -gt "$LENGTH" ]; then
        echo "Error: File path too long!"
        echo "Try moving this repository to a location inside fewer subdirectories"
        exit 1
    fi
    if ! [ "$(tail -c+"$OFFSET" "$1" | head -c16)" = DRI-DRIVERS-PATH ]; then
        echo "Error: sed or wc utility gave wrong result for file offset!"
        echo "Try checking locale settings, for example run this script with LC_ALL=C"
        exit 1
    fi
    printf "%s\0" "$2" | dd status=none iflag=fullblock of="$1" oflag=seek_bytes conv=notrunc seek="$((OFFSET - 1))" 
}

install_to() {
    if [ -d "$1" ]; then
        rm -r "$1"
    fi
    mkdir "$1"
    tar -C "$1" --strip-components=1 -xJf "$2"
    DRI_PATH="$(realpath "$1"/dri)"
    for file in "$1"/lib{gbm,EGL,GL}.so
    do
        patch_dri_path "$file" "$DRI_PATH"
    done
}

download_and_install() {
    mkdir -p versions

    if [ "$(uname -m)" = aarch64 ]; then
        INSTALL_AARCH64=1
    fi

    ARM_FILE="versions/panfork-$DATE-$VERSION-arm.tar.xz"
    AARCH64_FILE="versions/panfork-$DATE-$VERSION-aarch64.tar.xz"

    download_file arm "$ARM" "$ARMSHA" "$ARM_FILE"
    if [ "$INSTALL_AARCH64" ]; then
        download_file aarch64 "$AARCH64" "$AARCH64SHA" "$AARCH64_FILE"
    fi

    mkdir -p "$1"

    DRIVERS_PATH_MSG=

    install_to "$1"/arm "$ARM_FILE"
    if [ "$INSTALL_AARCH64" ]; then
        install_to "$1"/aarch64 "$AARCH64_FILE"
    fi

    if [ "$DRIVERS_PATH_MSG" ]; then
        echo "If it tries loading drivers from the wrong directory, try setting:"
        echo " LIBGL_DRIVERS_PATH=$(realpath -- "$1")/arm/dri:$(realpath -- "$1")/aarch64/dri"
    fi
}

add_ld_path() {
    if [ "$LD_LIBRARY_PATH" ]; then
        export LD_LIBRARY_PATH="$1":"$LD_LIBRARY_PATH"
    else
        export LD_LIBRARY_PATH="$1"
    fi
}

if [ "$1" = -h ] || [ "$1" = --help ]; then
    printf "Usage: %s [--revert|--version VER] [--install DEST] [--reinstall]\n" "$0"
    printf "   or: %s --oldver VER\n" "$0"
    printf "   or: %s --download-csv\n" "$0"
    printf "See README.md for more information.\n"
    exit
elif [ "$1" = --oldver ]; then
    VER="$2"
    shift 2

    set_version "$VER"
    set_vars
    print_version "Using version"
    if [ -d oldver/"$VER" ]; then
        echo Version is already installed
        printf "Remove %s to force reinstall.\n" "$(realpath oldver/"$VER")"
    else
        download_and_install oldver/"$VER"
    fi

    if [ "$1" == --shell ]; then
        add_ld_path "$(realpath oldver/"$VER"/arm)"
        add_ld_path "$(realpath oldver/"$VER"/aarch64)"
        return
    fi
    exit 0
elif [ "$1" == --download-csv ]; then
    download_versions
    exit 0
fi

if [ "$1" = --revert ]; then
    set_revert
    shift 1
elif [ "$1" = --version ]; then
    set_version "$2"
    shift 2
else
    download_versions

    set_latest
fi
set_vars
print_version "The latest version is"

if [ "$1" = --install ]; then
    INSTALLPATH="$2"
    shift 2
else
    INSTALLPATH=.
fi

if [ -e "$REFLOG" ] && [ "$(tail -n1 "$REFLOG")" = "$VERSION" ] && ! [ "$1" = --reinstall ] && [ -d "$INSTALLPATH"/arm ]; then
    echo Version is already installed
    echo Use --reinstall to force reinstall.
else
    download_and_install "$INSTALLPATH"
    echo "$VERSION" >>"$REFLOG"
fi
